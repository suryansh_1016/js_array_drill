function filter(elements, cb) {
  const result = [];

  if (Array.isArray(elements)) {
    if (elements.length == 0) {
      return "Passed Array is empty";
    } else {
      for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index])) result.push(elements[index]);
      }
      return result;
    }
  } else {
    return "Given Input is not an Array";
  }
}

module.exports = filter;
