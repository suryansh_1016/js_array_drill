function reduce(elements, cb, startingValue) {
  let accumulator =
    startingValue !== undefined ? startingValue : elements.shift();

  if (Array.isArray(elements)) {
    if (elements.length == 0) {
      return "Passed Array is empty";
    } else {
      for (let index = 0; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index]);
      }

      return accumulator;
    }
  } else {
    return "Given Input is not an Array";
  }
}
module.exports = reduce;
