function find(elements, cb) {
  if (Array.isArray(elements)) {
    if (elements.length == 0) {
      return "Passed Array is empty";
    } else {
      for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index)) {
          return elements[index];
        }
      }

      return "element not found";
    }
  } else {
    return "Given Input is not an Array";
  }
}

module.exports = find;
