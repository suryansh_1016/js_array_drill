const items = require("../items");
const reduce = require("../reduce");

const result = reduce(
  items,
  function (acc, currentValue) {
    return acc + currentValue;
  },
  0
);

console.log(result);
