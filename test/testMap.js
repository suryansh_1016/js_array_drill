const map = require("../map");
const items = require("../items");

const result = map(items, function (element, index) {
  return element > 2;
});

console.log(result);
