const filter = require("../filter");
const items = require("../items");

const result = filter(items, function cb(item) {
  return item % 2 == 0;
});

console.log(result);
