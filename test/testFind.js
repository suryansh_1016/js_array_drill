const find = require("../find");
const items = require("../items");

const result = find(items, function cb(elements, index) {
  return elements > 3;
});

console.log(result);
