const each = require("../each");
const items = require("../items");

each(items, function cb(element, index) {
  console.log(element, index);
});
