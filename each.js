function each(elements, cb) {
  // callback function here is used to log out the element and the index of the element.
  if (Array.isArray(elements)) {
    if (elements.length == 0) {
      return "Passed Array is empty";
    } else {
      for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
      }
    }
  } else {
    return "Given Input is not an Array";
  }
}

module.exports = each;
