function map(elements, cb) {
  const newArray = [];

  if (Array.isArray(elements)) {
    if (elements.length == 0) {
      return "Passed Array is empty";
    } else {
      for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index)) {
          newArray.push(elements[index]);
        }
      }
      return newArray;
    }
  } else {
    return "Given Input is not an Array";
  }
}

module.exports = map;
